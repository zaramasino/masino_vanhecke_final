There are multiple ways to run this program.

1. On one computer:
	Run with no arguments and it will run the server and both players on the same computer.
		ex: java -jar .\Masino_VanHecke_Final

2. On multiple computers:
	- On the computer/command line linked to the server, use one argument "server"
		ex: java -jar .\Masino_VanHecke_Final server
	- On the other computers, use three arguments: "player" "address" "playerNum"
		ex: java -jar .\Masino_VanHecke_Final player 127.0.0.1 2
		- substitute address with the address of your server
		- (!) playerNum must be 1 or 2
		- (!) it is case sensitive

There can only ever be two players, any additional players will not be linked to the gameplay even 
when the player number is 1 or 2.
	 