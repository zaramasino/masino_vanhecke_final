/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package BattleShipFinal;

import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The purpose of this class is to create a clip object that will play a
 * specific audio at various points throughout the game
 *
 * @author brijolievanhecke
 */
public class Clips {

    //Source: https://www.geeksforgeeks.org/play-audio-file-using-java/
    //Creates a private File object and sets it to null
    private File currentFile = null;
    private Clip clip = null;

    /**
     * This constructor creates a clip object and makes it so the file now
     * stores the path to the audio clip that was passed in via the parameter
     *
     * @param clipFileName
     */
    public Clips(String clipFileName) {
        currentFile = new File(clipFileName);
    }//Ends clips constructor

    /**
     * This method plays the audio clip currently being stored in the clip's
     * file object
     */
    public void playClip() {

        try {
            //The below command creates a new clip object
            clip = AudioSystem.getClip();
            //The below command gets the audio input stream currently stored in the 
            //currentFile object and stores that audio in the clip
            clip.open(AudioSystem.getAudioInputStream(currentFile));
            //The below command actually plays the audio
            clip.start();
        } catch (Exception e) {
            //Error handling
            e.printStackTrace();
        }//Ends catch

    }//Ends playClip method

}//Ends clip class
