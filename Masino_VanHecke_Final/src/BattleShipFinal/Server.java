/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package BattleShipFinal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * A class that provides the server side actions and allows us to communicate
 * between the two players during the game
 * Largely inspired by the class example csc2620_unit8_serverclientthreadexample
 * a chat based socket example
 *
 * @author brijolievanhecke
 */
public class Server extends Thread {

    //Declaration and Instantiation of data fields that will be used throughout the class
    private Socket connectionToBattleShipWindows = null;
    private ServerSocket server = null;
    private int port;
    private ArrayList<playerThread> threads = new ArrayList();
    private int playerCount = 0;

    /*
    The constructor for the server class. Assigns the port number passed in as
    the port for the server and creates a new server socket with the port number
    @param port, int
     */
    public Server(int port) throws IOException {
        //Sets new port number to the number passed in the parameters
        this.port = port;
        //Outputs information in regards to where the server is being run and the
        //port number
        System.out.println("Starting server on " + InetAddress.getLocalHost() + " on port " + port);
        try {
            //Connects new server socket 
            server = new ServerSocket(this.port);
            //Error handling
        } catch (IOException ex) {
            System.out.println("server socket error ");
        } catch (NullPointerException ex) {
            System.out.println("null pointer error ");
        }
    }

    /*
    The run method of the program which loops while the game is being played 
    and manages communications between the server and the players
     */
    @Override
    public void run() {
        //While the server exists
        while (true) {
            try {
                int maxConnections = 2;
                //Call accept on the server and set it to the connection socket
                if (playerCount == maxConnections) {
                    break;
                } else {
                    //Create a new playerthread on the same port which will allow the 
                    //players to receive and send messages, then start the thread
                    //and add it to a list of threads (this will allow the server to 
                    //send out messages to all of the players at once)
                    //Catch exception if there is an error with connecting to the server
                    connectionToBattleShipWindows = server.accept();
                    //create a player and start their thread
                    playerThread newThread = new playerThread();
                    newThread.start();
                    playerCount++;
                    threads.add(newThread);
                }

                //Prevents more than 2 players from being able to join the game
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Connection to client can't be "
                        + " established",
                        "Error", JOptionPane.ERROR_MESSAGE);
                break;
                //Catch exception if there is already a connection to the server
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(null, "A connection has already been"
                        + " established to the server. ",
                        "Error", JOptionPane.ERROR_MESSAGE);
                break;
            }
        }//Ends the while true loop
    }//Ends the run method

    /*
    A method that allows the server to send out messages to every player at once 
    using the post method
    @param line, String - the message to send
     */
    private void postMessage(String line) {
        for (int index = 0; index < threads.size(); index++) {
            //Sends message to all player threads
            threads.get(index).post(line);
        }
    }

    /*
    A class that allows the players to read from and write to the server
     */
    private class playerThread extends Thread {

        //Declaration and Instantiation of data fields that will be used throughout the class
        private BufferedReader in = null;
        private PrintWriter out = null;

        /*
        Constructor that sets up the player communication
        @param p: int, what does this even do??
         */
        private playerThread() {
            try {
                //Sets the in and out to a buffered reader and printwriter using 
                //the input and output streams of the connection
                in = new BufferedReader(new InputStreamReader(connectionToBattleShipWindows.getInputStream()));
                out = new PrintWriter(connectionToBattleShipWindows.getOutputStream());
                //error handling
            } catch (IOException ex) {
                System.out.println("Error - IOException");
            }
        }

        /*
        The run for player that is constantly listening for messages
         */
        @Override
        public void run() {
            try {
                while (true) {
                    //Set the line to an empty space to clear it
                    String line = "";
                    try {
                        //Set line to the input from the server
                        line = in.readLine();
                    } catch (IOException ex) {
                        //Catch this error if a player leaves unnexpectedly
                        System.out.println("A player has left");
                        postMessage("Player left");
                        System.exit(0);
                    } catch (NullPointerException exx) {
                        //Catch this error if a player leaves unnexpectedly
                        System.out.println("An error has occured, reboot server.");
                        System.exit(0);
                    }
                    //If there is input, so if line is not equal to ""
                    if (!line.equals("")) {
                        //Push the message to all of the player threads
                        postMessage(line);
                        //Reset line
                        line = "";
                    }
                }
                //Catches an error if the other player leaves unexpectedly or if
                //the server is shut down in the middle 
            } catch (NullPointerException exx) {
                System.out.println("An error has occured, reboot server.");
                //Exits out of the GUI
                System.exit(0);
                //source: https://coderanch.com/t/634573/java/properly-close-socket-connection 
                try {
                    //Closes the server
                    in.close();
                    out.close();
                    server.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }//Ends nullpointerexception 

        }//Ends the run method

        /*
        A method that just pushes a line to the output and then flushes it to clear it
        @param line: String, the message to send 
         */
        public void post(String line) {
            out.println(line);
            out.flush();
        }//Ends post method

    }//Ends playerthread class

}//Ends server class
