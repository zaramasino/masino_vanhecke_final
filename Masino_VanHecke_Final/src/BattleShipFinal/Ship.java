/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package BattleShipFinal;

import java.awt.Color;
import java.util.ArrayList;

/**
 * The purpose of this class is to create a ship object that has all of the
 * properties and behaviors of a ship
 *
 * @author brijolievanhecke
 */
public class Ship {

    //Creates and instantiates several private data properties for the class
    private int numOfCells;
    private int numOfHitCells;
    private boolean shipIsSunk;
    private boolean shipIsPlaced;
    private ArrayList<Cell> cellList;
    private int orientation;

    /**
     * This constructor creates a ship object that currently has no hit cells,
     * has an empty cellList, has no orientation, and is neither placed nor sunk
     *
     * @param x
     * @return
     */
    private Ship() {
        numOfHitCells = 0;
        shipIsSunk = false;
        shipIsPlaced = false;
        cellList = new ArrayList<Cell>();
        orientation = -1;
    }//Ends ship constructor

    /**
     * This method incorporates the factory design pattern into our project.
     * This method allows the user to avoid creating a ship object directly and
     * instead call buildShip on the actual ship class.
     *
     * @param x
     * @return
     */
    public static Ship buildShip(int x) {
        //Creates a new ship object
        Ship newShip = new Ship();
        //Sets the number of cells that this ship has
        newShip.setNumOfCells(x);
        //Returns the newShip
        return newShip;
    }//Ends buildShip method

    /**
     * Returns the cellList of the ship
     *
     * @return ArrayList<Cell>
     */
    public ArrayList<Cell> getCellList() {
        return cellList;
    }//Ends getCellList method

    /**
     * This method adds the cell the user clicked on to the ships cell list
     *
     * @param cell
     */
    public void addCellToList(Cell cell) {
        cellList.add(cell);
        cell.setToShip();
    }//Ends addCellToList method

    /**
     * This sets the orientation of the ship to either horizontal or vertical
     *
     * @param state
     */
    public void setOrientation(int state) {
        orientation = state;
    }//Ends setOrientation

    /**
     * This returns the orientation of the ship
     *
     * @return
     */
    public int getOrientation() {
        return orientation;
    }//ends getOrientation

    /**
     * This changes the status of the ship to either sunk or not sunk
     *
     * @param statusShip
     */
    public void setIsSunk(Boolean statusShip) {
        shipIsSunk = statusShip;
    }//Ends setIsSunk method

    /**
     * This returns whether the ship is sunk or not
     *
     * @return boolean shipIsSunk
     */
    public boolean getIsSunk() {
        return shipIsSunk;
    }//ends getIsSunk method

    /**
     * This method checks if a ship is placed and if it is placed then it will
     * turn all of the cells green
     *
     * @param isPlaced
     */
    public void setPlaced(Boolean isPlaced) {
        this.shipIsPlaced = isPlaced;
        //Checks if the ship is placed
        if (isPlaced == true) {
            for (int i = 0; i < cellList.size(); i++) {
                //Turns every single ship cell green
                cellList.get(i).getButton().setBackground(Color.green);
            }
        }
    }//ends setplaced method

    /**
     * Returns whether the ship is placed or not
     *
     * @return boolean
     */
    public boolean getIsPlaced() {
        return shipIsPlaced;
    }//ends getIsPlaced method

    /**
     * Returns the num of cells of a ship that have been hit
     *
     * @return int
     */
    public int getNumOfHitCells() {
        return numOfHitCells;
    }//ends getNumOfHitCells method

    /**
     * Sets the number of cells a ship has
     *
     * @param x
     */
    public void setNumOfCells(int x) {
        this.numOfCells = x;
    }//ends setNumOfCells method

    /**
     * Returns the number of cells a ship has
     *
     * @return int
     */
    public int getNumOfCells() {
        return numOfCells;
    }//ends getNumOfCells method

    /**
     * The purpose of this method is to check whether a ship has been sunk or
     * not based on whether or not the number of hit cells is equal to the
     * number of total cells the ship has
     *
     * @param coords
     */
    public void updateHitCells(String coords) {
        for (int index = 0; index < cellList.size(); index++) {
            if (cellList.get(index).getCoords().equals(coords)) {
                numOfHitCells++;
                //If the number of hit cells is equal to the number of cells a
                //ship has then the method will make the ship sunk
                if (numOfHitCells == numOfCells) {
                    setIsSunk(true);
                }//Ends if statement
            }//Ends nested if statement
        }//Ends for loop
    }//ends updateHitCells method

    /**
     * This cell determines whether a ship still has more cells that need to be
     * placed or not
     *
     * @return
     */
    public boolean needsCells() {
        if (cellList.size() < numOfCells) {
            return true;
        } else {
            return false;
        }
    }//ends needCells method

    /**
     * Goes through all of the cells that compose a ship and sets them to their
     * default color and icon. After that the entire cellList of the ship is
     * cleared
     */
    public void clearShip() {
        for (int index = 0; index < cellList.size(); index++) {
            cellList.get(index).setToDefault();
        }
        cellList.clear();

    }//ends clearShip method

    /**
     * This method cycles through all of the cells of a ship and changes them to
     * show the skull and bones icon
     */
    public void makeSunkIcon() {
        for (int index = 0; index < cellList.size(); index++) {
            //Creates a new icon object and displays the skull and bones icon 
            //that plays sound.
            icon newIcon = new icon(cellList.get(index).getButton(), 3, 1);
        }
    }//ends makeSunkIcon

    /**
     * Purpose: Helps to build a string that displays all necessary properties
     * of a ship object
     *
     * @return String
     */
    @Override
    public String toString() {
        return "ship info" + cellList.toString() + " " + numOfHitCells;
    }//Ends toString method
}//Ends ship class
