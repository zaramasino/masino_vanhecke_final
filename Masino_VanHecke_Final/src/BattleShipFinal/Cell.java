/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShipFinal;

import java.awt.Color;
import javax.swing.JButton;

/**
 *
 * The purpose of this class is to manage the data properties and the behavior
 * of a cell that makes up a game ship
 *
 * @author zara
 */
public class Cell {

    //Creates and instantiates several private data properties for the class
    private JButton button;
    private int xCoord;
    private int yCoord;
    private Color baseColor = new Color(131, 209, 232);
    private boolean isShip;
    private boolean isMissed;
    private boolean isHit;
    private boolean isPlaced;
    private boolean intact;
    private String coords;
    private boolean isSunk;
    private boolean enemySunk;
    private boolean enemyMiss;
    private boolean enemyHit;

    /**
     * This cell constructor creates a cell object and gives each cell a button,
     * an x and y coordinate, and sets all of the current booleans to false.
     *
     * @param butoon
     * @param x
     * @param y
     */
    public Cell(JButton cellButton, int x, int y) {
        button = cellButton;
        xCoord = x;
        yCoord = y;
        coords = "(" + x + ", " + y + ")";
        isShip = false;
        isMissed = false;
        isHit = false;
        isSunk = false;
        enemySunk = false;
        enemyMiss = false;
        enemyHit = false;
        intact = false;
    }//End cell constructor 

    /**
     * The purpose of this method is to reset a cell's properties to its
     * original state so that the user can reset the board and play again if
     * they want
     */
    public void resetCells() {
        isShip = false;
        isMissed = false;
        isHit = false;
        isSunk = false;
        enemySunk = false;
        enemyMiss = false;
        enemyHit = false;
        blankCell();
    }//End resetCells method

    /**
     * The purpose of this method is to reset a cell's external properties back
     * to its original state so that the color is light blue and there is no
     * icon displayed.
     */
    public void blankCell() {
        button.setBackground(baseColor);
        button.setIcon(null);
        button.setDisabledIcon(null);
    }//End blankcell method

    /**
     * Changes a player's cell intact boolean to true
     */
    public void setIsIntact() {
        intact = true;
    }//Ends setIsNeverhit method

    /**
     * Gets whether the player's cell has been fired on or not
     *
     * @return boolean
     */
    public boolean getIsIntact() {
        return intact;
    }//ends getIsIntact method

    /**
     * Makes the player's cell "sunk"
     */
    public void setIsSunkCell() {
        isSunk = true;
    }//Ends setIsSunkCell method

    /**
     * Gets whether the player's cell is sunk or not
     */
    public boolean getIsSunkCell() {
        return isSunk;
    }//Ends getIsSunkCell method

    /**
     * Makes a cell that belongs to an enemy "sunk"
     */
    public void setIsEnemySunkCell() {
        enemySunk = true;
    }//Ends setIsEnemySunkCell method

    /**
     * Gets whether a cell belonging to an enemy is sunk or not
     */
    public boolean getIsEnemySunkCell() {
        return enemySunk;
    }//Ends getIsEnemySunkCell method

    /**
     * Changes a cell that belongs to enemy to a "miss"
     */
    public void setIsEnemyMiss() {
        enemyMiss = true;
    }//Ends setIsEnemyMiss method

    /**
     * Gets whether a cell belonging to an enemy is a miss or not
     */
    public boolean getIsEnemyMiss() {
        return enemyMiss;
    }//Ends getIsEnemyMiss method

    /**
     * Changes a cell that belongs to enemy to a "hit"
     */
    public void setIsEnemyHit() {
        enemyHit = true;
    }//Ends setIsEnemyHit method

    /**
     * Gets whether a cell belonging to an enemy is a hit or not
     *
     * @return boolean
     */
    public boolean getIsEnemyHit() {
        return enemyHit;
    }//Ends getIsEnemyHit method

    /**
     * Changes a player's cell to be a "miss"
     *
     * @param choice
     */
    public void setIsMissed(boolean choice) {
        this.isMissed = choice;
    }//Ends setIsMissed method

    /**
     * Gets whether a player's cell is a miss or not
     *
     * @return
     */
    public boolean getIsMissed() {
        return isMissed;
    }

    /**
     * Changes a player's cell to be a "hit"
     *
     * @param choice
     */
    public void setIsHit(boolean choice) {
        this.isHit = choice;
    }

    /**
     * Gets whether a player's cell is a hit or not
     *
     * @return
     */
    public boolean getIsHit() {
        return isHit;
    }

    /**
     * Changes a regular cell so that it will now be considered a part of a ship
     */
    public void setToShip() {
        isShip = true;
    }

    /**
     * Returns whether a given cell is a ship cell or not
     *
     * @return
     */
    public boolean getIsShip() {
        return isShip;
    }

    /**
     * Changes whether a specific ship cell has been placed on the board yet
     *
     * @param choice
     */
    public void setIsPlaced(boolean choice) {
        this.isPlaced = choice;
    }

    /**
     * Returns whether a specific ship cell has been placed yet
     *
     * @return boolean
     */
    public boolean getIsPlaced() {
        return isPlaced;
    }

    /**
     * Gets the x coord of a cell
     *
     * @return int
     */
    public int getXCoord() {
        return xCoord;
    }

    /**
     * Gets the y coord of a cell
     *
     * @return int
     */
    public int getYCoord() {
        return yCoord;
    }

    /**
     * Returns both the x and y coordinates of a cell
     *
     * @return String
     */
    public String getCoords() {
        return coords;
    }

    /**
     * Changes a cell so that it goes back to it's original state. It will no
     * longer be considered a part of a ship.
     */
    public void setToDefault() {
        isShip = false;
        button.setBackground(new Color(131, 209, 232));
    }

    /**
     * Returns the button associated with a given cell
     *
     * @return JButton
     */
    public JButton getButton() {
        return button;
    }

    /**
     * Purpose: Helps to build a string that displays all necessary properties
     * of a cell object
     *
     * @return String
     */
    @Override
    public String toString() {
        return xCoord + " " + yCoord + " " + isShip + coords;
    }

}//Ends cell class
