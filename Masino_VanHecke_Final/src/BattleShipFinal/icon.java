/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShipFinal;

import java.awt.Color;
import javax.swing.JButton;

/**
 * The purpose of this class is to create an icon object that will display a
 * different icon and play a different audio clip based on what specific
 * parameters were passed in Source:
 * https://stackoverflow.com/questions/8980701/java-icon-image-is-grey-on-a-disabled-jbutton
 * We used the above link to figure out how to properly disable a button that
 * showed an icon
 *
 * @author brijolievanhecke
 */
public class icon {

    private final Clips explosionClip = new Clips("src/soundEffects/miniExplosionn.wav");
    private final Clips missedExplosion = new Clips("src/soundEffects/missedExplosion.wav");

    /**
     * This method displays a different icon and play a different audio clip
     * based on what specific parameters were passed in
     *
     * @param button
     * @param x
     * @param y
     */
    public icon(JButton button, int x, int y) {

        //If x ==1 then the program should display an icon and audio clip that
        //indicates that the player hit a ship
        if (x == 1) {
            //Makes the button display an explosion icon
            button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/explosionIcon.png")));
            button.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/explosionIcon.png")));
            //Sets buttons color to red
            button.setBackground(Color.red);
            //If y ==1 then the miniexplosion sound effect should play as well
            if (y == 1) {
                explosionClip.playClip();

            }
            //If x ==2 then the should display an icon and audio clip that
            //indicates that the player did not hit a ship
        } else if (x == 2) {
            //Makes the button display a miss icon
            button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/missIcon.png")));
            button.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/missIcon.png")));
            //If y ==1 then the missedExplosion sound effect should play as well
            if (y == 1) {
                missedExplosion.playClip();
            }
            //If x ==3 then the should display an icon and audio clip that
            //indicates that the player sunk a ship
        } else if (x == 3) {
            //Makes the button display a skull and bones icon
            button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/skullandbones.png")));
            button.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/skullandbones.png")));
            button.setBackground(Color.black);
            //If y ==1 then the miniExplosion sound effect should play as well
            if (y == 1) {
                explosionClip.playClip();
            }
        }
    }//Ends icon constructor

}//Ends icon class
