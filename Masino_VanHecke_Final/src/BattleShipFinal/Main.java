/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShipFinal;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UnsupportedLookAndFeelException;
import BattleShipFinal.Player;

/**
 * The main driving class that establishes our server and our two players
 * source: https://stackoverflow.com/questions/7341683/parsing-arguments-to-a-java-command-line-program
 * @author zara
 */
public class Main {

    //the string for the server address
    public static String ipAddress = "";
    //the title of player for each player
    public static String name = "Player ";

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        //checks for when the program is run, provides feedback to user on what
        //arguments to use
        if(args.length>=1){
            //checks that the correct args are used to run the server
            if (args[0].equals("server") && args.length==1) {
                System.out.println("server is running");
                //checks that the proper args are used for players
            } else if (args[0].equals("player")) {
                if (args.length < 3) {
                    System.out.println("Must provide 3 args, player, address, number");
                    System.exit(0);
                }
                //checks that the player names are valid
                if (args.length == 3) {
                    ipAddress = args[1];
                    if(args[2].equals("1") || args[2].equals("2")){
                        name += args[2];
                    }
                    //informs the user that the player number must be 1 or 2
                    else{
                        System.out.println("Player number must be 1 or 2");
                        System.exit(0);
                    }
                }
                //tells you that there is something wrong with the arguments
            } else {
                System.out.println("Arguments are wrong, check readme");
                System.exit(0);
            }
        }
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            java.awt.EventQueue.invokeLater(new Runnable() {

                //This run method will continue to loop until the server is closed
                @Override
                public void run() {
                    try {
                        //if there are no command line arguments, run everything on one machine
                        if (args.length == 0) {
                            //Creates the server with the port 5009 then starts it
                            Server server = new Server(5009);
                            server.start();
                            //Creates a player with the local computer address, the port 5009
                            //and the name player 1 & then starts the player thread
                            Player p1 = new Player("127.0.0.1", 5009, "Player 1");
                            p1.start();
                            //Creates a player with the local computer address, the port 5009
                            //and the name player 2 & then starts the player thread
                            Player p2 = new Player("127.0.0.1", 5009, "Player 2");
                            System.out.println("player 2 id: " + p2.getId());
                            p2.start();
                            //if one argument is passed in, aka server
                        } else if (args.length == 1) {
                            //create the server on port 5009 then start it
                            Server server = new Server(5009);
                            server.start();
                        } //if there are 3 arguments, then you are a player trying to connect
                        else if (args.length == 3) {
                            //create a player with the address, port 5009, and the name 
                            //then start it
                            Player p = new Player(ipAddress, 5009, name);
                            p.start();
                            //otherwise say that the args are invalid
                        } else {
                            System.out.println("invalid args length");
                        }

                        //Error handling
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (UnsupportedLookAndFeelException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }//Ends the run method
            });//Ends the runnable class
            //More try-catches to prevent errors from shutting down the entire program
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BattleShipGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BattleShipGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BattleShipGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BattleShipGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }//Ends main method
}//Ends main class
