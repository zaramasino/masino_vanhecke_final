/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShipFinal;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

/**
 * The purpose of this method is to create a field object that will allow users
 * to place and visually represent where their own ships. Additionally, the
 * field class will give user's the ability to fire on their enemy's ships and
 * identify hits/ misses through visual changes
 *
 * Source: https://github.com/cosenary/Battleship/tree/master/battleship The
 * grid layout/clicking functionality was aided greatly by the link above
 *
 * @author zara
 */
public class Field extends JPanel {

    //declares and instantiates the data properties that will be used throughout
    //the rest of this class
    private Boolean placingShips;
    private ArrayList<String> validH = new ArrayList<>();
    private ArrayList<String> validV = new ArrayList<>();
    private Ship currentShip;
    private int shipIndex;
    private JPanel fieldPanel = new javax.swing.JPanel();
    private Color baseColor = new Color(131, 209, 232);
    private ArrayList<Cell> allCells = new ArrayList<>();
    private ArrayList<Ship> allShips = new ArrayList<>();
    private ArrayList<Cell> hitEnemyShipCells = new ArrayList<>();
    private ArrayList<Cell> missedEnemyShipCells = new ArrayList();
    private Cell currentCell = null;
    private boolean newCurrentCell;
    private String currentBoard = "ownBoard";
    private final Clips placeShipSound = new Clips("src/soundEffects/placeShip.wav");
    ;
    private icon icon;
    private GridBagConstraints c;

    /**
     * The buildFields method creates and returns the field panel where the
     * players will place their ships and also fire upon their enemies ships.
     *
     * @param frame
     * @param updateTextField
     * @param clearLastShipButton
     * @return
     */
    public JPanel buildFields(JFrame frame, JTextField updateTextField, JButton clearLastShipButton) {
        clearLastShipButton.setEnabled(false);

        //This clearLastShip action listener allows the user to clear multiple
        //ships they have already placed
        clearLastShipButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //If the currentship is not equal to null and the user is still
                //placing ships then this if statement will be triggered
                if (currentShip != null && placingShips == true) {
                    //If the currentship has not been entirely placed 
                    if (currentShip.getIsPlaced() != true) {
                        //Clear the ship and switch the currentship to a ship 
                        //in the previous index
                        currentShip.clearShip();
                        currentShip = allShips.get(shipIndex);
                        currentShip.setPlaced(true);
                    } else {//If the currentship has been placed 
                        //Swap the currentship to a ship of a previous index in the 
                        //array list
                        shipIndex--;
                        currentShip = allShips.get(shipIndex);
                        currentShip.clearShip();
                        currentShip.setPlaced(true);
                        //Displays text to indicate that a ship was cleared
                        updateTextField.setText("Ship cleared, place a " + currentShip.getNumOfCells() + " cell ship");
                        if (shipIndex == 0) {
                            clearLastShipButton.setEnabled(false);
                        }
                    }
                }//Ends if statement
            }//Ends actionPerformed method
        });//Ends mouse action listener

        //Sets placing ships to true and adds several ships to the allShips arraylist
        placingShips = true;
        allShips.add(Ship.buildShip(4));
        allShips.add(Ship.buildShip(3));
        allShips.add(Ship.buildShip(2));
        allShips.add(Ship.buildShip(1));
        allShips.add(Ship.buildShip(2));
        allShips.add(Ship.buildShip(3));
        //Makes it so that the currentship starts off as the first ship in the arraylist
        shipIndex = 0;
        currentShip = allShips.get(shipIndex);
        try {

            //Changes the design and look of the buttons in the feld class
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println(e);
        }

        //Gives the fieldPanel a gridbag layout, makes the color a light blue,
        //and sets the dimensions and border of the panel
        fieldPanel.setLayout(new GridBagLayout());
        fieldPanel.setPreferredSize(new Dimension(340, 340));
        fieldPanel.setBackground(baseColor);
        fieldPanel.setBorder(BorderFactory.createLineBorder(new Color(32, 156, 185)));

        c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;

        for (int col = 0; col < 10; col++) {

            for (int row = 0; row < 10; row++) {

                JButton button = new JButton();
                button.setBackground(baseColor);
                button.setBorder(BorderFactory.createLineBorder(new Color(32, 156, 185)));
                button.setCursor(new Cursor(Cursor.HAND_CURSOR));
                button.setPreferredSize(new java.awt.Dimension(32, 32));
                Cell newCell = new Cell(button, col, row);
                allCells.add(newCell);

                //Whenever the button, located in a newCell, is clicked then this mouse
                //listener will be triggered
                button.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent event) {
                        //This gets the information of the button that was clicked
                        JButton cellButton = newCell.getButton();

                        //If statement that is triggered if the button is not enabled
                        if (newCell.getButton().isEnabled() == false) {
                            return;
                            //If the button is not enabled then the program breaks out 
                            //of this method
                        }
                        //Stores the button that was clicked in this new cellbutton
                        cellButton = (JButton) event.getSource();

                        //If the user is still placing ships then this method will
                        //be triggered
                        if (placingShips == true) {
                            //Changes the text in the textfield to ask the user
                            //to place a ship with a certain number of cells
                            updateTextField.setText("Please place a " + currentShip.getNumOfCells() + " cell ship.");

                            //If the user the clicked on is already a ship then 
                            //the textfield will remind the user that they can't 
                            //place there
                            if (newCell.getIsShip() == true) {
                                updateTextField.setText("Ship has already been placed at this position");

                            } else {//This else statement is triggered if the newCell
                                //the user clicked is not already a ship

                                //If they currentShip still needs cells to be placed
                                //by the user then this if will be triggered
                                if (currentShip.needsCells() == true) {

                                    if (currentShip.getCellList().size() == 0) {
                                        //If no cells of a currentship have been placed
                                        //other than the one selected then the
                                        //user's next valid spots include anything 
                                        //adjacent to the newCell
                                        clearValids();
                                        validV.add(toCoord(newCell.getXCoord(), newCell.getYCoord() - 1));
                                        validV.add(toCoord(newCell.getXCoord(), newCell.getYCoord() + 1));
                                        validH.add(toCoord(newCell.getXCoord() - 1, newCell.getYCoord()));
                                        validH.add(toCoord(newCell.getXCoord() + 1, newCell.getYCoord()));
                                        //calls the placeShip newCell method which
                                        //visually changes the cells color to either
                                        //pink or green depending on if the entire
                                        //ship has been placed yet or not
                                        placeShipCell(newCell, clearLastShipButton);

                                        //If one newCell has been placed for the current ship and the user clicks on another newCell
                                    } else if (currentShip.getCellList().size() == 1) {
                                        //if the clicked newCell coordinates is in either valid list
                                        if (validH.contains(newCell.getCoords()) || validV.contains(newCell.getCoords())) {
                                            //if the x coordinate of that newCell matches the x coord of the first newCell in the ship
                                            if (newCell.getXCoord() == currentShip.getCellList().get(0).getXCoord()) {
                                                //set the orientation to 1, which signifies vertical and remake the valid list using 
                                                //that cells information
                                                currentShip.setOrientation(1);
                                                vUpdateValid(newCell.getXCoord(), newCell.getYCoord(), newCell);
                                            } else {
                                                //in the other case, if the y coords match then set the orientation to 0 (horizontal)
                                                //and reevaluate the horizontal list with the given newCell
                                                currentShip.setOrientation(0);
                                                hUpdateValid(newCell.getXCoord(), newCell.getYCoord(), newCell);
                                            }
                                            //place a ship in that newCell
                                            placeShipCell(newCell, clearLastShipButton);
                                            //if the newCell is not in those lists, then it is not an adjacent newCell and is therefore invalid
                                        } else {
                                            updateTextField.setText("Invalid cell placement, cells must be adjacent.");
                                        }

                                        //this occurs when 2 or more cells of a ship have been placed
                                        //so the orientation should be established
                                    } else {
                                        //if the current ship orientation is vertical
                                        if (currentShip.getOrientation() == 1) {
                                            //if the valid vertical list contains the coords of the newCell clicked
                                            if (validV.contains(newCell.getCoords())) {
                                                //update the valid newCell list with that new newCell information
                                                vUpdateValid(newCell.getXCoord(), newCell.getYCoord(), newCell);
                                                //place a ship in that newCell
                                                placeShipCell(newCell, clearLastShipButton);
                                                //if the coords are not in the vertical valid list then inform the user
                                            } else {
                                                updateTextField.setText("Invalid cell placement, cells must be vertically adjacent.");
                                            }
                                            //if the orientation is horizontal
                                        } else if (currentShip.getOrientation() == 0) {
                                            //if the coords are in the valid horizontal placement list
                                            if (validH.contains(newCell.getCoords())) {
                                                //update the horizontal list and place a ship in that newCell
                                                hUpdateValid(newCell.getXCoord(), newCell.getYCoord(), newCell);
                                                placeShipCell(newCell, clearLastShipButton);
                                                //inform the user that their choice was invalid
                                            } else {
                                                updateTextField.setText("Invalid cell placement, cells must be horizontally adjacent.");
                                            }
                                        }
                                    }

                                    //If the current ship being looked at no longer needs
                                    //any cells to be placed then the ship
                                    //will be declared as placed and the program
                                    //will switch to the next ship
                                    if (currentShip.needsCells() != true) {
                                        currentShip.setPlaced(true);
                                        clearValids();
                                        swapShips(clearLastShipButton, updateTextField);
                                    }
                                }
                            }

                        } else { //This else is triggered when the player is 
                            //no longer placing ships
                            //If a selected newCell was already fired upon then
                            //the program will let the user know that is not a 
                            //valid spot to fire on

                            if (newCell.getIsHit() == true || newCell.getIsMissed() == true || newCell.getIsSunkCell() == true) {
                                updateTextField.setText("Misfire, please fire again.");

                            } else {
                                //If the newCell selected has not already been fired on
                                //then the new current newCell will be updated to
                                //reflect that
                                currentCell = newCell;
                                newCurrentCell = true;
                            }
                        }
                    }
                });

                c.gridx = col;
                c.gridy = row;
                //Adds each button to a specific grid in the field panel
                fieldPanel.add(button, c);
            }
        }
        return fieldPanel;
    }//Ends the buildField method

    /**
     * Returns all of the ships
     *
     * @return ArrayList<Ship>
     */
    public ArrayList<Ship> getAllShips() {
        return allShips;
    }

    /**
     * Returns the boolean that indicates whether a player is still placing
     * ships or not
     *
     * @return boolean
     */
    public boolean getPlacingShips() {
        return placingShips;
    }

    /**
     * Returns the index of the currentship being looked from the allShips array
     * list
     *
     * @return int
     */
    public int getIndex() {
        return shipIndex;
    }

    /**
     * Returns whether there is a new current newCell or not
     *
     * @return boolean
     */
    public boolean getNewCurrentCell() {
        return newCurrentCell;
    }

    /**
     * Sets whether there is a new current newCell or not
     *
     * @param response
     */
    public void setNewCurrentCell(boolean response) {
        newCurrentCell = response;
    }

    /**
     * Returns an array list of all of the cells a player has fired on that were
     * misses
     *
     * @return ArrayList<Cell>
     */
    public ArrayList<Cell> getMissedEnemyShipCells() {
        return missedEnemyShipCells;
    }

    /**
     * Returns all of the players ships
     *
     * @return ArrayList<Ship>
     */
    public ArrayList<Ship> getMyShips() {
        return allShips;
    }

    /**
     * Returns an array list of all of the cells a player has fired on that were
     * HITS
     *
     * @return ArrayList<Cell>
     */
    public ArrayList<Cell> getHitEnemyShipCells() {
        return hitEnemyShipCells;
    }

    /**
     * Return the current newCell that was just clicked on
     *
     * @return Cell
     */
    public Cell getCurrentCell() {
        return currentCell;
    }

    /**
     * Makes the current newCell equal to null
     */
    public void setCurrentCellNull() {
        currentCell = null;
    }

    /**
     * Returns every single newCell in a player's field
     *
     * @return ArrayList<Cell>
     */
    public ArrayList<Cell> getAllCells() {
        return allCells;
    }

    /**
     * Creates a "game board" which visually represents where a player has
     * hit/missed/sunk ships on their opponent's board
     */
    public void gameBoard() {
        currentBoard = "gameboard";
        //The for loop will continue until there are no more cells to cycle through
        for (int index = 0; index < allCells.size(); index++) {

            //If a newCell was sunk then the newCell's icon & color will visually change
            //to represent a sunk newCell
            if (allCells.get(index).getIsSunkCell() == true) {
                icon = new icon(allCells.get(index).getButton(), 3, 0);

                //If a newCell was hit then the newCell's icon & colorwill visually change
                //to represent a hit newCell
            } else if (allCells.get(index).getIsHit() == true && allCells.get(index).getIsSunkCell() != true) {
                icon = new icon(allCells.get(index).getButton(), 1, 0);

                //If a newCell was missed by the player then the newCell's icon will visually change
                //to represent a missed newCell    
            } else if (allCells.get(index).getIsMissed() == true) {
                icon = new icon(allCells.get(index).getButton(), 2, 0);

                //If a newCell was neither hit nor missed then the newCell 
                //will turn light grey to indicate that a player didn't manage
                //to hit these ships
            } else if (allCells.get(index).getIsIntact() == true) {
                allCells.get(index).getButton().setBackground(Color.white);
                allCells.get(index).getButton().setIcon(null);
                //If a newCell was not a ship newCell and was not missed then the newCell
                //will have no icon and be blue
            } else {
                allCells.get(index).blankCell();
            }
        }
    }

    /**
     * * Creates a "your board" panel which visually represents where a player
     * has placed their own ships and which of their own ships have been
     * hit/sunk/missed by the opponent player
     */
    public void yourShipBoard() {
        currentBoard = "ownBoard";
        //This for loop cycles through all of the cells until there are no more
        for (int index = 0; index < allCells.size(); index++) {

            //If an enemy has sunk a newCell then the icon & color will be changed
            //to reflect that
            if (allCells.get(index).getIsEnemySunkCell() == true) {
                icon = new icon(allCells.get(index).getButton(), 3, 0);

                //If an enemy has hit a newCell then the icon & color will be changed
                //to reflect that
            } else if ((allCells.get(index).getIsEnemyHit() == true) && allCells.get(index).getIsEnemySunkCell() != true) {
                icon = new icon(allCells.get(index).getButton(), 1, 0);

                //If an enemy has missed a newCell then the icon & color will be changed
                //to reflect that
            } else if (allCells.get(index).getIsEnemyMiss() == true) {
                icon = new icon(allCells.get(index).getButton(), 2, 0);

                //If an enemy has neither hit nor missed a newCell and there is a ship
                //there then the newCell will be shown as green and have no icon
                //to reflect that
            } else if (allCells.get(index).getIsShip() == true) {
                allCells.get(index).getButton().setBackground(Color.green);
                allCells.get(index).getButton().setIcon(null);

                //If a newCell is not a ship newCell and it hasn't been hit/missed/sunk
                //by an enemy then the color well remain blue and no icon will
                //be displayed
            } else {
                allCells.get(index).blankCell();
            }
        }//Ends the for loop
    }//Ends the yourShipBoard method

    /**
     * Returns a blank board panel where the cells are all blue and there are no
     * icons all the cells booleans are reset to the default as well
     */
    public void blankBoard() {
        for (int i = 0; i < allCells.size(); i++) {
            allCells.get(i).resetCells();
        }
    }

    /**
     * Disables every button in each of the cells
     */
    public void turnOffButtons() {
        for (int i = 0; i < allCells.size(); i++) {
            allCells.get(i).getButton().setEnabled(false);
        }
    }

    /**
     * Enables every button in each of the cells
     */
    public void turnOnButtons() {
        for (int i = 0; i < allCells.size(); i++) {
            allCells.get(i).getButton().setEnabled(true);
        }
    }

    /**
     * If a player managed to hit a ship newCell from their opponent then the
     * newCell will be added to their hit list & the newCell's icon and color
     * will change
     *
     * @param cell
     */
    public void updateHit(Cell cell) {
        cell.setIsHit(true);
        icon = new icon(cell.getButton(), 1, 1);
        hitEnemyShipCells.add(cell);
    }

    /**
     * If a player missed ship newCell on their opponent's board then the
     * newCell will be added to their miss list and the newCell's icon will
     * change
     *
     * @param cell
     */
    public void updateMiss(Cell cell) {
        cell.setIsMissed(true);
        icon = new icon(cell.getButton(), 2, 1);
        missedEnemyShipCells.add(cell);
    }

    /**
     * If a player sunk their opponents ship at a certain newCell then the will
     * be manually set to sunk and the icon & color will change to represent a
     * sunk newCell
     *
     * @param cell
     */
    public void updateSunkCell(Cell cell) {
        cell.setIsSunkCell();
        icon = new icon(cell.getButton(), 3, 1);
    }

    /**
     * This method switches the currentship being looked at
     *
     * @param clearLastShipButton
     * @param updateTextField
     */
    public void swapShips(JButton clearLastShipButton, JTextField updateTextField) {
        //If the index of the current ship is less than 5 than this means that
        //all of the ships have  not been placed yet
        if (getIndex() < 5) {
            //Switches current ship to the next ship in the array list
            shipIndex++;
            currentShip = allShips.get(shipIndex);
            //The 
            currentShip.setPlaced(true);
            updateTextField.setText("Please place a " + currentShip.getNumOfCells() + " cell ship.");

            //If the index of the current ship is greater than 5 than this means that
            //all of the ships have  been placed yet and it's time for the other player
            //to place their ships or for the game to officially start
        } else {

            //This JOPtion pane asks the user to confirm the locations of their placed
            //ships
            //Source: https://stackoverflow.com/questions/14407804/how-to-change-
            //the-default-text-of-buttons-in-joptionpane-showinputdialog
            updateTextField.setText(" ");
            Object[] choices = {"Confirm Ship Placement", "Clear Ships"};
            Object defaultChoice = choices[0];
            int result = JOptionPane.showOptionDialog(this,
                    "Would you like to confirm your ship placement or clear your ships?",
                    "",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    choices,
                    defaultChoice);

            //If the user chose to confirm where they placed their ships then their
            //placing ships boolean will be set to false and they will no longer
            //be able to engage with placing ships clicking functionality
            if (result == JOptionPane.YES_OPTION) {
                placingShips = false;
                turnOffButtons();
                //message
                currentShip = null;
                clearLastShipButton.setEnabled(false);

                //If the user chose to clear all of their placed ships then the index
                //will be set back to 0, the currentship will point to the first ship
                //in the array list, and the user will maintain all of the clicking
                //functionaliy with regard to placing ships
            } else {
                JOptionPane.showMessageDialog(null, "Clearing all ships");
                clearShips(clearLastShipButton, updateTextField);
                shipIndex = 0;
                currentShip = allShips.get(shipIndex);
                updateTextField.setText("All ships cleared, begin replacing with a ship with 4 cells.");
            }
        }//Ends else
    }//Ends swap ships method

    /**
     * This method adds a newCell that has been placed by a user into the
     * current ship's celllist, changes it's color, and triggers the placing
     * ships audio clip
     *
     * @param cell
     * @param clearLastShipButton
     */
    public void placeShipCell(Cell cell, JButton clearLastShipButton) {
        //Adds the newCell to the currentShips newCell list
        currentShip.addCellToList(cell);
        clearLastShipButton.setEnabled(true);
        //Makes the newCell pink
        cell.getButton().setBackground(Color.pink);
        //Plays the placing ships audio clip  
        placeShipSound.playClip();
        currentShip.setPlaced(false);
    }

    /**
     * Transforms a cells x and y coordinates into a string and then returns
     * that string
     *
     * @param x
     * @param y
     * @return
     */
    public String toCoord(int x, int y) {
        String coord = "(" + x + ", " + y + ")";
        return coord;
    }

    /**
     * A method that keeps track of all the valid horizontal placements for a
     * ship newCell during the placing ships portion of the game
     *
     * @param x
     * @param y
     * @param cell
     */
    public void hUpdateValid(int x, int y, Cell cell) {
        //check that the given x and y won't push off of the grid
        if (x >= 0 & x <= 9 & y >= 0 & y <= 9) {
            //if the newCell passed in exists in the validH list already, then add 
            //the cells to the left and right of it as valid too
            if (validH.contains(cell.getCoords())) {
                validH.add(toCoord(x - 1, y));
                validH.add(toCoord(x + 1, y));
            }
        }
    }

    /**
     * A method that keeps track of all the valid vertical placements for a ship
     * newCell during the placing ships portion of the game
     *
     * @param x
     * @param y
     * @param cell
     */
    public void vUpdateValid(int x, int y, Cell cell) {
        //check that the given x and y won't push off of the grid
        if (x >= 0 & x <= 9 & y >= 0 & y <= 9) {
            //if the newCell passed in exists in the validV list already, then add 
            //the cells to the top and bottom of it as valid too
            if (validV.contains(cell.getCoords())) {
                validV.add(toCoord(x, y + 1));
                validV.add(toCoord(x, y - 1));
            }
        }
    }

    /**
     * Clears both of the valid placement lists at once
     */
    public void clearValids() {
        validH.clear();
        validV.clear();
    }

    /**
     * Returns the current board state (gameboard or your own ship board)
     *
     * @return
     */
    public String getCurrentBoard() {
        return currentBoard;
    }

    /**
     * If a user chooses to clear all of their ships then this method will be
     * triggered. It will cycle through all of the ships and clear each of the
     * ship's cell lists
     *
     * @param clearLastShip
     * @param updateTextField
     */
    public void clearShips(JButton clearLastShip, JTextField updateTextField) {
        for (int index = 0; index < allShips.size(); index++) {
            allShips.get(index).clearShip();
            clearLastShip.setEnabled(false);
        }
    }//Ends clearShips method
}//Ends the field class
