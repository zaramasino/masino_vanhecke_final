/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BattleShipFinal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * The purpose of this class is to create an object that has all of the
 * properties and behaviors of a player and can communicate with another player
 * during a game of battleship
 * Largely inspired by the class example csc2620_unit8_serverclientthreadexample
 * a chat based socket example
 * 
 * @author zara
 */
public class Player extends Thread {

    //Declaration and instantiation of all of the private data properties we will
    //use throughout this class
    private BattleShipGUI gui;
    private String address;
    private int portNum;
    private String name;
    private Socket connectionToServer = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private JTextField updateTextField;
    private String message = "";
    private boolean gameOver = false;
    private boolean donePlacingShips = false;
    private boolean startGame = false;
    private int counter = 0;
    private boolean yourTurn = false;
    private Field field;
    private ArrayList<Cell> allCells;
    private ArrayList<Ship> myShips;

    /*
    constructor for player, it takes in all the information for the server connection
    and sets some of the defaults for a player
    @param address: String, the computer address
    @param portNumber: int, the port to connect to
    @param name String, the name for the player
     */
    public Player(String address, int portNumber, String name) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        this.name = name;
        this.address = address;
        this.portNum = portNumber;
        gui = new BattleShipGUI(name);
        updateTextField = gui.getTextField();
        field = gui.getField();
        allCells = field.getAllCells();
        myShips = field.getMyShips();
        System.out.println(name + " connecting to " + address + " on port " + portNumber);
    }

    /*
    A method that sends a string from the player to the server in order to communicate with
    the other player and then flushes the output
    @param message: String, the message to send
     */
    public void sendMessage(String message) {
        out.println(message);
        out.flush();
    }

    /*
    Gives the textfield a new string to display
    @param textField: String
     */
    public void setTextField(String textField) {
        updateTextField.setText(textField);
    }

    /*
     A debug method used to check that messages are properly sent and recieved
     */
    private void debug(String message) {

        if (name.contains("Player 2")) {
            System.out.println("     debug" + getId() + " " + name + " " + "$$" + message + "$$");

        } else {
            System.out.println("debug" + getId() + " " + name + " " + "$$" + message + "$$");
        }

    }

    /*
    A method that calls sleep in order to slow down some while loops that were 
    previously too fast for the program to handle
     */
    public void sleepShort() {

        try {
            sleep(100);

        } catch (InterruptedException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    A method that handles all of the logic for firing on a cell
     */
    public void fireLogic() {
        //If the message is coming from the other player
        if (!message.contains(name)) {
            //If the message contains fire, assume hit is false to begin with
            //The current player is being asked whether the other player managed
            //to hit a cell that contains one of their ships in it
            if (message.contains(" fire")) {
                boolean hit = false;
                //Check your own ships to see if the player has a ship cell 
                //with coords that match the coords sent in the message
                for (int i = 0; i < myShips.size(); i++) {
                    for (int x = 0; x < myShips.get(i).getCellList().size(); x++) {
                        //If a ship does contain the same cell coords of the cell sent over
                        // in the message, then update your own board  to indicate
                        //that the other player has made a hit a this location
                        if (message.contains(myShips.get(i).getCellList().get(x).getCoords())) {
                            myShips.get(i).updateHitCells(myShips.get(i).getCellList().get(x).getCoords());
                            //Check if all of the cells in that ship have been sunk and
                            //if they haven't, update the textfield and view to indicate
                            //the ship still hasn't been fully sunk yet. Then
                            //send a message including your name, hit, and the 
                            //coords of that cell then set hit to true
                            if (myShips.get(i).getIsSunk() != true) {
                                myShips.get(i).getCellList().get(x).setIsEnemyHit();
                                updateTextField.setText("You have been hit at " + myShips.get(i).getCellList().get(x).getCoords() + ".");
                                gui.updateView();
                                gui.setGameStatus("You have been hit at " + myShips.get(i).getCellList().get(x).getCoords() + ".");
                                sendMessage(name + " hit " + myShips.get(i).getCellList().get(x).getCoords());
                                message = "";
                                hit = true;
                                //If all the cells are sunk then send a message to the
                                //player indicating that they have sunk a ship
                                //and give your ship cells that were sunk
                                //a skull and cross icon
                            } else if (myShips.get(i).getIsSunk() == true) {
                                //Set coords to an empty string
                                String coords = "";
                                //Get all of the cell coords and send a message with your name,
                                // the fact that it was sunk, and the coords of each
                                //shipp cell that was sunk for this ship.
                                //Reset both the coords and the message and set hit to true
                                for (int a = 0; a < myShips.get(i).getCellList().size(); a++) {
                                    myShips.get(i).getCellList().get(a).setIsEnemySunkCell();
                                    //This is Just formatting to get the grammar right in the textfield
                                    if (myShips.get(i).getCellList().size() == 1) {
                                        coords += myShips.get(i).getCellList().get(a).getCoords();
                                    } else if (myShips.get(i).getCellList().size() == 2) {
                                        if (a < (myShips.get(i).getCellList().size() - 1)) {
                                            coords += myShips.get(i).getCellList().get(a).getCoords() + " ";
                                        } else {
                                            coords += "and " + myShips.get(i).getCellList().get(a).getCoords();
                                        }
                                    } else {
                                        if (a < (myShips.get(i).getCellList().size() - 1)) {
                                            coords += myShips.get(i).getCellList().get(a).getCoords() + ", ";
                                        } else {
                                            coords += "and " + myShips.get(i).getCellList().get(a).getCoords();
                                        }
                                    }
                                }
                                //Update the textfield and the view to display
                                //that your ship was sunk
                                updateTextField.setText("Your ship was sunk at these cells: " + coords + ".");
                                gui.setGameStatus("Your ship was sunk at these cells: " + coords + ".");
                                gui.updateView();
                                sendMessage(name + " sunk " + coords);
                                message = "";
                                coords = "";
                                hit = true;
                            }
                        }
                    }
                }//End for loop
                //If hit is still false, so no cell was hit, send a message that
                //they missed with the coords of that cell
                if (hit == false) {
                    //Update the textfield and set coords to an empty string
                    updateTextField.setText("You missed. ");
                    gui.setGameStatus("You missed. ");
                    String coords = "";
                    //For every cell, check if their coords match the ones in the 
                    //message and if they do, add them to coords and set them to
                    //an enemy miss
                    for (int index = 0; index < allCells.size(); index++) {
                        if (message.contains(allCells.get(index).getCoords())) {
                            coords = allCells.get(index).getCoords();
                            allCells.get(index).setIsEnemyMiss();
                        }
                    }
                    //Update the view and the textfield
                    gui.updateView();
                    //If the current board state is gameboard, then turn on the buttons
                    updateTextField.setText("Enemy missed at " + coords + ", " + name + " begin firing. ");
                    gui.boardField.gameBoard();
                    gui.setGameStatus("Enemy missed at " + coords + ", " + name + " begin firing. ");
                    gui.getShowOwnBoardButton().setText("View your ships  ");
                    gui.setShowOwnField(false);
                    if (field.getCurrentBoard().equals("gameboard")) {
                        field.turnOnButtons();
                    }
                    //Enable the show own button, set turn to true, and send a message 
                    //with your name, miss, and the coords of the cell that was missed
                    //Reset message
                    gui.enableShowOwnBoardButton();
                    yourTurn = true;
                    gui.setYourTurn(true);
                    sendMessage(name + " miss " + coords);
                    message = "";
                    //--------------------------------------
                }
            }
            //If the message contains hit and came from the other player then
            //this means that you have managed to hit a cell on your enemy's gameboard
            if (message.contains(" hit")) {
                //Update that cell in your game board to show that you have hit
                //an enemy ship at that location
                for (int index = 0; index < allCells.size(); index++) {
                    if (message.contains(allCells.get(index).getCoords())) {
                        field.updateHit(allCells.get(index));
                    }
                }
                //Update the textfield saying that you hit something, reset the newcurrentcell to false
                //Enable the showboard button, and set your turn to true again
                updateTextField.setText("Hit at: " + field.getCurrentCell().getCoords() + ". " + name + ", fire again!");
                gui.setGameStatus("Hit at: " + field.getCurrentCell().getCoords() + ". " + name + ", fire again!");
                field.setNewCurrentCell(false);
                yourTurn = true;
                gui.setYourTurn(true);
                sendMessage(name + " MOVE MADE HIT -----------------------------------------");
            }
            //If the message contains miss and comes from the other playerthen
            //this means that you did not hit a cell on your enemy's gameboard
            if (message.contains(" miss")) {
                //Update that cell as missed, change the textfield, turn off the buttons
                //and set the newcurrentcell to false again
                for (int index = 0; index < allCells.size(); index++) {

                    if (message.contains(allCells.get(index).getCoords())) {
                        field.updateMiss(allCells.get(index));
                    }
                }
                updateTextField.setText("You missed, your turn is over.");
                gui.setGameStatus("You missed, your turn is over.");
                field.turnOffButtons();
                field.setNewCurrentCell(false);
                sendMessage(name + " MOVE MADE MISS ---------------------------------------");
            }
            //If the message contains "sunk" &  the other player's name then
            //this means that you have managed to sink an enemy's entire ship
            if (message.contains(" sunk")) {
                //Go through and update all the cells that are contained in the 
                //message as coords and update your board so their booleans 
                //indicate that they are sunk
                //Keep track of the total number that the enemy has sunk as well
                //as the cells that havent been sunk yet
                int numOfEnemySunkCells = 0;
                ArrayList<String> intactShipCells = new ArrayList();
                //For every single cell
                for (int y = 0; y < allCells.size(); y++) {
                    //If the coords of the cells you are checking are in the
                    //message then set them to sunk and update them 
                    //condense here??
                    if (message.contains(allCells.get(y).getCoords())) {
                        allCells.get(y).setIsSunkCell();
                        field.updateSunkCell(allCells.get(y));
                    }
                    //If the cell is a ship and it is not sunk, then add the coords
                    //to the intact ship cells list
                    if (allCells.get(y).getIsShip() == true && allCells.get(y).getIsEnemySunkCell() == false) {
                        intactShipCells.add(allCells.get(y).getCoords());
                    }
                    //If that cell is sunk, increment the number of sunk cells
                    if (allCells.get(y).getIsSunkCell()) {
                        numOfEnemySunkCells++;
                    }
                }
                //If the total number of sunk cells is 15 then all the ships are sunk
                //and a messae should be sent to end the game and indicate that 
                //the player has lost
                if (numOfEnemySunkCells == 15) {
                    //Send a message with your name, game over, and the ship cells that
                    //were not hit
                    //Update the textfield and set gameover to true
                    sendMessage(name + " game over in not your name " + intactShipCells);
                    updateTextField.setText("You won!");
                    gui.setGameStatus("Win");
                    //Change the field to the end of game board, disable the clear button, and turn off
                    //the buttons
                    //field.yourShipBoard();
                    //field.endOfGameBoard();
                    field.gameBoard();
                    //gui.disableShowOwnBoardButton();
                    gui.disableClearLastShipButton();
                    field.turnOffButtons();
                    //Otherwise, update the textfield, set newcurrentcell to false, turn the board
                    //button back on, and set your turn to true again
                } else {
                    String coords = "";
                    String[] parts = message.split("\\(");
                    for (int index = 1; index < parts.length; index++) {
                        coords += "(" + parts[index];
                    }
                    updateTextField.setText("Ship sunk at: " + coords + ". " + name + " Fire again!");
                    gui.setGameStatus("Ship sunk at: " + coords + ". " + name + " Fire again!");
                    field.setNewCurrentCell(false);
                    yourTurn = true;
                    gui.setYourTurn(true);
                    sendMessage(name + " MOVE MADE HIT -----------------------------------------");
                }
            }
            //If the message has game over from the other player then tihs means
            //the current player has won and that the game should end
            if (message.contains(" game over")) {
                //Go through the cells and if their coords are in the message
                //then set them to still intact
                for (int y = 0; y < allCells.size(); y++) {
                    if (message.contains(allCells.get(y).getCoords())) {
                        allCells.get(y).setIsIntact();
                    }
                }
                //Update the textfield, trigger the game over board, set the 
                //gameover boolean in gui to true, enable the board button, disable the
                //clear last ship button, turn off all the buttons,
                //and set gameover to true
                updateTextField.setText("You lost! :( ");
                gui.setGameStatus("Lose");
                //field.endOfGameBoard();
                field.gameBoard();
                field.turnOffButtons();
                gameOver = true;
                field.turnOffButtons();
                sendMessage("did something here");
            }//End if message contains "game over" if statement
        }
    }//Ends fireLogic method

    /*
    Run method for player which tracks the state of the game and sends messages 
    between two players
     */
    @Override
    public void run() {
        try {
            //Connect the player socket using the address and portnumber, set up the
            //input and output streams and send a message saying that you are connected
            connectionToServer = new Socket(address, portNum);
            in = new BufferedReader(new InputStreamReader(connectionToServer.getInputStream()));
            out = new PrintWriter(connectionToServer.getOutputStream());
            message = "connected";
            sendMessage(message);
            message = "";
            //If your name is player 2, then swap to the gameboard and disable the buttons.
            //Inform the player that they are waiting for player 1 to place their ships
            if (name.equals("Player 2")) {
                field.gameBoard();
                field.turnOffButtons();
                updateTextField.setText("Waiting for other player to place ships...");
                //Otherwise, tell player 1 to place their ships
            } else {
                updateTextField.setText("Please place a 4 cell ship.");
            }
            //Disable the show own button
            gui.disableShowOwnBoardButton();
            //Catch exceptions

            while (true) {
                //While the game is still running
                while (gameOver == false) {
                    //Try to read a message from the server
                    try {
                        message = in.readLine();
                        //Error handling  
                    } catch (SocketException ex) {
                        JOptionPane.showMessageDialog(gui.getFrame(), "There has been a "
                                + "server error or a player has left"
                                + ", please check your server and play again");
                        //source: https://coderanch.com/t/634573/java/properly-close-socket-connection
                        try {
                            out.close();
                            in.close();
                            connectionToServer.close();
                            //Error handling  
                        } catch (IOException ep) {
                            System.out.println("Error closing the socket and streams");
                            //Error handling  
                        } catch (NullPointerException otherException) {
                            System.out.println("Error closing the socket and streams");
                        }
                        System.exit(0);
                        //Error handling  
                    } catch (IOException ex) {
                        System.out.println("Other player has left");
                        System.exit(0);
                    }
                    //If the game play has not started yet
                    if (startGame == false) {
                        //If your name is player 1
                        if (name.equals("Player 1")) {
                            //If the message you recieve contains Player 2's name and "start game"
                            //then inform the player that they can fire, turn on their buttons,
                            //set the start game boolean to true and the your turn boolean to true
                            if ((!message.contains(name)) && message.contains(" start game")) {
                                updateTextField.setText("Please begin firing");
                                gui.setGameStatus("Please begin firing");
                                //field.gameBoard();
                                field.turnOnButtons();
                                startGame = true;
                                yourTurn = true;
                                gui.setYourTurn(true);
                                //Else if the message does not contain s"tartvgame" from player 2
                            } else {
                                //While you are still placing your ships, keep the 
                                //player thread waiting/stuck in one place
                                while (field.getPlacingShips() == true) {
                                    sleepShort();
                                }
                                //Once the player is done placing ships, 
                                //check if doneplacingships is false
                                if (donePlacingShips == false) {
                                    //Swap to the gameboard, disable the clear last ship button,
                                    //send a message that this player is done, update the user,
                                    //and set the done placing boolean to true
                                    field.gameBoard();
                                    gui.disableClearLastShipButton();
                                    sendMessage(name + " done placing" + counter++);
                                    updateTextField.setText("Waiting for Player 2 to place ships");
                                    donePlacingShips = true;
                                    updateTextField.setText("Waiting for other player to place ships...");
                                    message = "";
                                }
                            }
                        }//End if name equals "player 1" if statement
                        //If the player's name is player 2
                        if (name.equals("Player 2")) {
                            //If the message contains the other players name and "done placing"
                            if ((!message.contains(name)) && message.contains(" done placing")) {
                                //Update the user, change the field to show the ship board, 
                                //turn on the board's buttons, and disable the clearlast ship button
                                updateTextField.setText("Please place a 4 cell ship");
                                field.yourShipBoard();
                                field.turnOnButtons();
                                gui.disableClearLastShipButton();
                                //While you are still placing your ships, sleep
                                while (field.getPlacingShips() == true) {
                                    sleepShort();
                                }
                                //When done placing ships, make the doneplacing boolean true, 
                                //swap to the gameboard, enable the show own button, 
                                //update the user that you are waiting on the other player now,
                                //send a message to start the game, and set the startgame to true
                                donePlacingShips = true;
                                //field.blankBoard();
                                field.gameBoard();
                                gui.enableShowOwnBoardButton();
                                // gui.disableShowOwnBoardButton();
                                updateTextField.setText("Waiting on other player...");
                                sendMessage(name + " start game");
                                startGame = true;
                                message = "";
                            }
                        }//Ends if the players name is equal to player 2 if statement
                    }//Ends if startgame == false if statement
                    //If the startgame boolena is true
                    if (startGame == true) {
                        //If it is the current player's turn then enable the show own board button
                        if (yourTurn == true) {
                            //field.gameBoard();
                            //field.turnOnButtons();
                            gui.enableShowOwnBoardButton();
                            //While the player still hasn't clicked a new cell, sleep
                            while (field.getNewCurrentCell() == false) {
                                sleepShort();
                            }
                            //Set the your turn boolean to false and send a message with the current player's
                            //name, the coords of the cell, and "fire"
                            yourTurn = false;
                            gui.setYourTurn(false);
                            sendMessage(name + " " + field.getCurrentCell().getCoords() + " fire");
                            message = "";
                        }
                        //Call the fire logic method and then sleep for a little bit
                        fireLogic();
                        sleepShort();
                    }//Ends the startgame == true boolean
                    //Reset message to an empty space
                    message = "";
                }//End while game over !=true loop
            }//End while true loop
            //This catch exception occurs when there is no server being run but
            //a player socket is attempting to connect to noe
        } catch (NullPointerException | SocketException ex) {
            //Shows a dialog option that lets them know the other player has
            //left the game. If they click okay or close the joptionpane then
            //the socket will be closed, the buffered reader will be closed, 
            //and the printwrtier will be closed to
            JOptionPane.showMessageDialog(gui.getFrame(), "There has been a server error or a player has left"
                    + ", please check your server and play again");
            //source: https://coderanch.com/t/634573/java/properly-close-socket-connection
            try {
                out.close();
                in.close();
                connectionToServer.close();
                //Error handling  
            } catch (IOException ep) {
                System.out.println("Error closing the socket and streams");
                //Error handling  
            } catch (NullPointerException otherException) {
                System.out.println("Error closing the socket and streams");
            }
            System.exit(0);
            //Error handling  
        } catch (IOException ex) {
            System.out.println("No server to connect to");
            System.exit(0);
            //This exception is caught if a player randomly leaves in the middle of
            //the game
        }
    }//End run method
}//End player class
